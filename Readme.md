## Advantages of working with Hibernate
- Queries will be generated
- Developers need not write the SQL query
- Maps Java objects to records in the table
- attributes of the class will be transalted to columns of the table
- Hibernate will internally do some optimization 
    - caching - First level cache 
    - Generates the SQL query 
    - Provide optional second level cache 
  

## How to setup Hibernate 
- Using Maven dependency
  
## Steps to setup a Hibernate project
1. Create a maven project          - Infrastructure
2. Add Hibernate dependency        - Infrastructure
3. Add the Mysql connector jar     - Infrastructure
4. Create a file called `hibernate-cfg.xml` inside `src/main/resources` directory
5. Hibernate-cfg.xml contains the DB details
   1. Drivername
   2. username
   3. password
   4. database name

6. Hibernate creates one object called `SessionFactory`


## Steps to work with the application
1. SessionFactory is one object per JVM 
2. From the SessionFactory create a session
3. Using the session, create a transaction
4. Commit the transaction
5. In case of any exception, rollback the transaction

## Hibernate-cfg.xml file

```xml
<!DOCTYPE hibernate-configuration PUBLIC
        "-//Hibernate/Hibernate Configuration DTD 3.0//EN"
        "http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd">
<hibernate-configuration>
    <session-factory>
        <property name="hibernate.show_sql">true</property>
        <property name="hibernate.format_sql">true</property>
        <property name="hibernate.dialect">org.hibernate.dialect.MySQL57Dialect</property>
        <property name="hbm2ddl.auto">create</property>
        <property name="connection.username">root</property>
        <property name="connection.driver_class">com.mysql.cj.jdbc.Driver</property>
        <property name="connection.password">welcome</property>
        <property name="connection.url">jdbc:mysql://localhost:3306/emp_db</property>

        <mapping class="com.classpath.hibernate.model.User"/>
        <mapping class="com.classpath.hibernate.model.Address"/>
        <mapping class="com.classpath.hibernate.model.Vehicle"/>
        <mapping class="com.classpath.hibernate.model.Course"/>
        <mapping class="com.classpath.hibernate.model.RegularEmployee"/>
        <mapping class="com.classpath.hibernate.model.ContractEmployee"/>
        <mapping class="com.classpath.hibernate.model.Employee"/>
    </session-factory>
</hibernate-configuration>
```

Create an `Entity` class called `Item`
```java
@Entity
@Table(name = "item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "item_name")
    private String name;

    private double price;

    public Item(){}

    public Item(long id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
```

Update the `hibernate-cfg.xml` file with the `Item` class

Write the `Client` class
```java
public class Client {
    public static void main(String[] args) {
        // 1. build the SessionFactory
        SessionFactory sessionFactory = new Configuration()
                    .configure("hibernate-cfg.xml").buildSessionFactory();

        //2. Create the session from sessionfactory
        Session session = sessionFactory.openSession();

        //3. Begin the transaction
        Transaction transaction = session.beginTransaction();

        Item item = new Item();
        item.setName("I-Phone 11");
        item.setPrice(1_10_000);

        try {
            //4. call the save method on session passing the item
            session.save(item);
            transaction.commit();
        }catch (Exception exception){
            //5. In case of exception rollback the transaction
            transaction.rollback();
        } finally{
            session.close();
        }
    }
}
```

Run the Program and validate the table in the database


