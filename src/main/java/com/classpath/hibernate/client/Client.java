package com.classpath.hibernate.client;

import com.classpath.hibernate.model.Item;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Client {
    public static void main(String[] args) {
        // 1. build the SessionFactory
        SessionFactory sessionFactory = new Configuration()
                    .configure("hibernate-cfg.xml").buildSessionFactory();

        //2. Create the session from sessionfactory
        Session session = sessionFactory.openSession();

        //3. Begin the transaction
        Transaction transaction = session.beginTransaction();

        Item item = new Item();
        item.setName("I-Phone 11");
        item.setPrice(1_10_000);

        try {
            //4. call the save method on session passing the item
            session.save(item);
            transaction.commit();
        }catch (Exception exception){
            //5. In case of exception rollback the transaction
            transaction.rollback();
        } finally{
            session.close();
        }
    }
}